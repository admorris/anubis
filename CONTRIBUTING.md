# Contributing

## Implementation

Generally, this package is a PyROOT-based set of utilities.
It is assumed that code written using this package will be executed in an environment with a full installation of ROOT and PyROOT.
Plotting is done with `TCanvas`, `TObject::Draw` etc.
Fitting is done with `RooFit`.
Machine learning is done with `TMVA`.

Other packages used are `particle` and `hepunits` from SciKit-HEP.

### Data processing

Use `RDataFrame` wherever supported.
If there is experimental support, particularly if it only exists in the `master` branch of ROOT, please add the implementation with a boolean flag to control its use.
For actions which require the implementation of a C++ function, please implement this in a `.C` macro that is loaded using `importlib.resources.path` and `gROOT.LoadMacro()`.

### Plotting

Plotting functions should draw existing objects on a `TCanvas` and save it to a file, where the base filename and list of extensions are arguments.

Don't bother adjusting style minutiae like axis ticks, font typefaces/sizes, margins etc.
Assume that the user will fix this themselves by manipulating `gStyle`, e.g. with `LHCbStyle.C`.

### Console commands

Console commands consisting of Python code running in the "analysis" environment should be implemented by adding another `subparser` to the `parse_args()` function in `__main__.py`.
The commands are then executable by running `anubis <command>`.
This removes the need to add every command to the entry points in `setup.cfg`.

Console commands consisting of Python code running under other environments (e.g. `lb-dirac`) should be written as a bash script that redirects a string to the correct Python interpreter, e.g.:
```bash
lb-dirac python << EOF
from DIRAC.Core.Base import Script
Script.parseCommandLine()
# ...
EOF
```

## Style

Generally the Python style should follow PEP-8 with the exception of the maximum line length being 120 chars
This is enforced in the `pre-commit` config.

### Function names

Function names should be some kind of verb/imperative statement (e.g. `get_bin_width`, `fit_data`, `make_hists`, `append_mva_score`)

- `get` and `set`: retrieve or change a class property (or some trivial transformation of it).
- `read` and `write`: reading/writing objects to/from a file.
- `make`: create and return some non-trivial object(s).
- `plot`: draw objects and save the canvas to a file (for creating drawable objects, still use `make`).
- `add`: add columns to a dataset (i.e. return `df.Define(...)`).
- `filter`: remove entries from a dataset (i.e. return `df.Filter(...)`).
- `train`, `fit`, `opt`: parameter-optimisation routines. Should return a on object containing the results and/or write them to a file.
  - `train`: implies machine learning models.
  - `fit`: implies probability density functions.
  - `opt`: miscellaneous optimisation (e.g. a cut value).

### Documentation

Python code should be documented with Google-style docstrings and PEP-484 type hinting.

C++ code should be documented with Doxygen comments.

Documentation web pages will be generated with Sphinx+Breathe.
