# Anubis

**An**alysis **u**tilities for LHC**b** phys**i**c**s**

**A**dam's **n**eat **u**tilities for LHC**b** analys**is**

*Acronym first, name later!*

## What is this?

A collection of generic utilities for common tasks in LHCb data analysis.


