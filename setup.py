from __future__ import absolute_import, division, print_function

from setuptools import setup

# This is required to allow editable pip installs while using the declarative configuration (setup.cfg)
setup()