import argparse
from .bookkeeping.utils import print_dict, get_all_eventtypes

def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest="action", required=True)

    print_dict_parser = subparsers.add_parser("print_bk_dict", description="Print a JSON dict containing the BK locations")
    print_dict_parser.add_argument("filename", nargs="+", help = "Plaintext files containing the output of dirac-bookkeeping-decays-path")
    print_dict_parser.add_argument("--config", default="dataset.json", help="Dataset metadata file")
    print_dict_parser.set_defaults(func=lambda args: print_dict(args.config, args.filename))

    eventtypes_parser = subparsers.add_parser("print_eventtypes", description="Print all event types in a dataset metadata file")
    eventtypes_parser.add_argument("--config", default="dataset.json", help="Dataset metadata file")
    eventtypes_parser.add_argument("--sim-only", action="store_true", help="Only print MC event types")
    eventtypes_parser.set_defaults(func=lambda args: print_all_eventtypes(args.config, args.sim_only))

    args = parser.parse_args()
    args.func(args)

def print_all_eventtypes(filename, sim_only=False):
    for e in get_all_eventtypes(filename, sim_only):
        print(e)