#!/usr/bin/env python
import json, re, os, sys, shutil, pkg_resources, argparse

def raise_parser_error(quantity, bk_path):
	raise ValueError(f"Unable to extract {quantity} from the following bookkeeping path:\n{bk_path}")

def get_year(bk_path):
	data_type, year_folder = bk_path.split("/")[1:3]
	if data_type == "MC":
		return year_folder
	elif data_type == "LHCb":
		return year_folder.replace("Collision", "20")
	else:
		raise_parser_error("data year", bk_path)

def get_polarity(bk_path):
	for polarity in "Up", "Down":
		if f"Mag{polarity}" in bk_path:
			return polarity
	raise_parser_error("magnet polarity", bk_path)

def ignore(bk_path):
	with open("bk_ignore", "r") as ignore_file:
		for pattern in ignore_file.readlines():
			pattern = pattern.strip()
			if re.search(pattern, bk_path) is not None:
				return True
	return False

def merge_duplicates(dups):
	entry = {key: sum([dup[key] for dup in dups]) for key in ["nFiles","nEvts"]}
	for key in "path", "DDDB", "CondDB":
		if len(set([dup[key] for dup in dups])) == 1:
			entry[key] = dups[0][key]
		else:
			problem_keys = [d[key] for d in dups]
			raise RuntimeError(f"{key} not all same in list of duplicates: {problem_keys}")
	entry["ProdIDs"] = [dup["ProdIDs"][0] for dup in dups]
	return entry

def find_merge_duplicates(entries):
	new_entries = []
	unique_paths = set([prod["path"] for prod in entries])
	for path in unique_paths:
		duplicates = [prod for prod in entries if prod["path"] == path]
		if len(duplicates) == 0:
			raise RuntimeError("The list of duplicate paths is empty. This should be impossible")
		elif len(duplicates) == 1:
			new_entry = duplicates[0]
		else:
			try:
				new_entry = merge_duplicates(duplicates)
			except RuntimeError as e:
				print(f"ERROR: ignoring {path} because {e}", file=sys.stderr)
				return []
		new_entries += [new_entry]
	return new_entries

def create_path_list(bk_locations_list):
	path_list = []
	for path, DDDB, CondDB, nFiles, nEvts, ProdID in bk_locations_list:
		if path[-3:] != "DST":
			print(f"ERROR: ignoring {path} because it does not end in 'DST'", file=sys.stderr)
			continue
		entry = {
			"path": str(path),
			"DDDB": str(DDDB),
			"CondDB": str(CondDB),
			"nFiles": int(nFiles),
			"nEvts": int(nEvts),
			"ProdIDs": [int(ProdID)],
		}
		path_list += [entry]
	return find_merge_duplicates(path_list)

def print_dict(database, filenames):
	bk_locations = {}
	for filename in filenames:
		eventtype = re.search(r"\d{8}", filename).group(0)
		with open(filename, "r") as f:
			lines = filter(None, f.read().splitlines())
			bk_locations_list = [l.split(" ") for l in set(lines) if "None" not in l and not ignore(l.split(" ")[0])]
			bk_locations[eventtype] = {
				"name": get_name(database, eventtype),
				"paths": sorted([l["path"] for l in create_path_list(bk_locations_list)]),
			}
	with open("bk_locations.json", "w") as f:
		json.dump(bk_locations, f, indent = 2)

def get_all_eventtypes(database, sim_only = False):
	dataset = json.load(open(database, "r"))
	eventtypes = []
	for sample in dataset:
		for eventtype in dataset[sample]["EventType"]:
			if not (sim_only and eventtype >= 90000000):
				eventtypes += [eventtype]
	return set(eventtypes)

def get_name(database, eventtype):
	dataset = json.load(open(database, "r"))
	for sample in dataset:
		if int(eventtype) in dataset[sample]["EventType"]:
			return sample
	raise RuntimeError(f"EventType {eventtype} not found in {database}")

def initialise():
	for filename in ["Makefile", "bk_ignore", "dataset.json"]:
		if os.path.exists(filename):
			print(f"WARNING: file {filename} already exists. Will not overwrite", file=sys.stderr)
		else:
			shutil.copy(pkg_resources.resource_filename(__name__, filename), os.getcwd())
			print(f"Created file {filename}")

