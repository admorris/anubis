#!/usr/bin/env python

import ROOT

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__),".."))
from plotting.utils import *

def fit_and_plot(pdf, data, fitvar, fit_options, component_config):
	c, mainpad, pullpad = create_plot_pull_canvas()
	pdf.fitTo(data, ROOT.RooFit.NumCPU(8), *fit_options)
	plot = fitvar.frame()
	plot.SetTitle("")
	data.plotOn(plot)
	for name in component_config:
		curve = pdf.plotOn(plot, ROOT.RooFit.Components(name), *component_config[name]).getCurve()
		curve.SetPoint(0,curve.GetXaxis().GetXmin(),0) # Work around ROOT plotting bug
		curve.addPoint(curve.GetXaxis().GetXmax(),0)
	pdf.plotOn(plot)
	data.plotOn(plot)
	pullplot = fitvar.frame()
	pullhist = plot.pullHist()
	pullplot.addPlotable(pullhist, "B")
	mainpad.cd()
	plot.Draw()
	plot.GetYaxis().SetTitle(plot.GetYaxis().GetTitle().replace("Events", "Candidates"))
	for axis in [plot.GetXaxis(), plot.GetYaxis()]:
		stylemainaxis(axis)
	plot.GetXaxis().SetLabelSize(0)
	pullpad.cd()
	stylepulls(pullhist, pullplot)
	pullplot.Draw()
	unit = fitvar.getUnit()
	pullplot.GetXaxis().SetTitle(plot.GetXaxis().GetTitle().replace(f"({unit})", f"[{unit}]"))
	plot.GetXaxis().SetTitle("") # Obscured by pull plot but visible when highlighting the PDF
	return c

