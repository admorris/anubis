from math import log10, ceil, floor
from copy import copy

def order(x):
	return int(floor(log10(abs(x))))

def round_sf(x, nsf):
	return round(x, nsf - order(x) - 1)

def format_dp(x, ndp):
	return (f"{{x:.{ndp}f}}").format(x = x)

def pdg_round(v, e):
	"""
	See section 5.3 https://pdg.lbl.gov/2020/reviews/rpp2020-rev-rpp-intro.pdf
	"""
	val = copy(v)
	err = copy(e) # paranoid about python's "everything is a reference"
	ov = order(val)
	oe = order(err)
	e3sf = int(floor(err * 10**(3 - int(ceil(log10(abs(err))))))) # first 3 s.f of the error as 3-digit int
	if e3sf < 100:
		raise ValueError(f"Cannot perform PDG rounding with err = {err}") # most likely err = 0
	elif e3sf < 355:
		nesf = 2
	elif e3sf < 950:
		nesf = 1
	else:
		err = round_sf(err, 1)
		nesf = 2
		oe += 1
	ndp = nesf - 1 - oe if oe < 0 else 0
	nvsf = nesf + ov - oe
	val = round_sf(val, nvsf)
	err = round_sf(err, nesf)
	return format_dp(val, ndp), format_dp(err, ndp)
