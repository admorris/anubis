#!/usr/bin/env python
import os, sys, glob, re, json, argparse, pkg_resources

def is_simulation(evttype):
	return int(evttype) < 90000000

def write_options_file(filename, content):
	filepath = os.path.join("options", filename)
	if not os.path.exists(filepath):
		f = open(filepath, "w")
		f.write(content)
		f.close()
	return filepath

def make_conditions(conds):
	CondDB = conds[0]
	DDDB = conds[1]
	filename = f"{CondDB}_{DDDB}.py"
	content = f"""
	from Configurables import DaVinci
	DaVinci().CondDBtag = "{CondDB}"
	DaVinci().DDDBtag = "{DDDB}"
	""".replace("\t", "")
	return write_options_file(filename, content)

def make_evtmax(evtmax):
	filename = f"EvtMax_{evtmax}.py"
	content = f"""
	from Configurables import DaVinci
	DaVinci().EvtMax = "{evtmax}"
	""".replace("\t", "")
	return write_options_file(filename, content)

def make_skipevts(skipevts):
	filename = f"SkipEvents_{skipevts}.py"
	content = f"""
	from Configurables import DaVinci
	DaVinci().SkipEvents = {skipevts}
	""".replace("\t", "")
	return write_options_file(filename, content)

def get_options_file(filename):
	return pkg_resources.resource_filename(__name__, os.path.join("options", filename))

def build_metadata_options(year, simulation, conds, filename, evtmax = -1, skipevts = 0):
	options = [
		get_options_file("SilenceRawEventFormatConf.py"),
		get_options_file("Output.py"),
		get_options_file(f"DataType-{year}.py"),
	]
	if simulation:
		options += [get_options_file("Simulation.py")]
		options += [make_conditions(conds)]
	else:
		options += [get_options_file("NoSimulation.py")]
		options += [get_options_file("DataTags.py")]
	for ext in ["LDST", "MDST", "RDST", "SDST", "XDST", "DST"]:
		if ext in filename:
			options += [get_options_file(f"InputType-{ext}.py")]
			break
	if evtmax >= 0:
		options += [make_evtmax(evtmax)]
	if skipevts > 0:
		options += [make_skipevts(skipevts)]
	return options

def get_nickname(dataset_meta, evttype):
	if int(evttype) >= 90000000:
		return "data"
	else:
		for nickname in dataset_meta:
			if int(evttype) in dataset_meta[nickname]["EventType"]:
				return nickname
	print(f"Event type {evttype} not found in metadata", file=sys.stderr)
	return "MC"

def create_job_name(nickname, evttype, year, polarity, bkpath):
	stripping = "S"+str(re.search("Stripping([rp\d]*)", bkpath).group(1))
	name = "_".join([nickname, str(evttype), str(year), polarity, stripping])
	# Append generator info to job name
	if is_simulation(evttype): name += "_" + str(re.search("Sim\d\d[^/]*", bkpath).group(0))
	return name

def create_jobs(args):
	jobs = [] # The thing we will be building
	# Open the JSON files containing the dataset metadata and the bookkeeping locations
	dataset_meta = json.load(open(args.dataset_meta, "r"))
	bk_locations = json.load(open(args.bk_locations, "r"))
	# Deduce set of polarities
	polarities = ["up", "down"] if args.polarity == "both" else [args.polarity]
	# Create the jobs
	for evttype in args.evttype:
		if str(evttype)  not in bk_locations:
			print(f"Skipping {evttype}: not found in {args.bk_locations}", file=sys.stderr)
			continue
		nickname = get_nickname(dataset_meta, evttype)
		for year in args.year:
			if str(year) not in bk_locations[str(evttype)]:
				print(f"Skipping {year} for {evttype}: not found in {args.bk_locations}", file=sys.stderr)
				continue
			for polarity in polarities:
				if polarity.capitalize() not in bk_locations[str(evttype)][str(year)]:
					print(f"Skipping {year} Mag{polarity} for {evttype}: not found in {args.bk_locations}", file=sys.stderr)
					continue
				for ds in bk_locations[str(evttype)][str(year)][polarity.capitalize()]:
					# Create the job
					jobs += [{
						"name": create_job_name(nickname, evttype, year, polarity, ds["path"]),
						"optionfiles": build_metadata_options(year, is_simulation(evttype), (ds["CondDB"], ds["DDDB"]), ds["path"].split("/")[-1], args.evtmax, args.skipevts) + args.optionfiles,
						"bk_path": ds["path"],
						"inputfiles": args.extrafiles,
						"postprocessor": get_options_file("merge.py"),
					}]
	with open(args.output, "w") as f:
		json.dump(jobs, f, indent = 2)

def main():
	parser = argparse.ArgumentParser(description = "Submit DaVinci jobs using Ganga")
	parser.add_argument("evttype", type = int, nargs = "+", help = "Event type (use 90000000 for data)")
	parser.add_argument("--year", type = int, nargs = "+", default = [2011, 2012, 2015, 2016, 2017, 2018], help = "List of years")
	parser.add_argument("--polarity", default = "both", choices = ["up", "down", "both"], help = "Magnet polarity")
	parser.add_argument("--extrafiles", nargs = "+", default = [], help = "Extra files to upload with job")
	parser.add_argument("--optionfiles", nargs = "+", default = [], help = "Extra options files to pass to DaVinci")
	parser.add_argument("--ignoremissing", action="store_true", help="Ignore missing files (for the splitter)")
	parser.add_argument("--evtmax", type = int, default = -1, help = "Max number of events per subjob: -1 for no limit")
	parser.add_argument("--skipevts", type = int, default = 0, help = "Number of events to skip per file")
	parser.add_argument("--bk-locations", default = "../dataset/bk_locations.json", help = "Path to JSON file storing the BK locations, created by the Anubis ntupling tools")
	parser.add_argument("--dataset-meta", default = "../dataset/dataset.json", help = "Path to JSON file storing the dataset metadata used by the Anubis ntupling tools")
	parser.add_argument("--output", default = "job_options.json", help = "Output file to write to")
	args = parser.parse_args()
	create_jobs(args)

