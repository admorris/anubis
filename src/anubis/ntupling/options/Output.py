from Configurables import DaVinci
DaVinci().HistogramFile = 'DVhists.root'
DaVinci().TupleFile = "DVntuple.root"
DaVinci().PrintFreq = 100000
from Configurables import LHCbApp
LHCbApp().XMLSummary="summary.xml"
