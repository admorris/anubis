"""
CustomChecker postprocessor which creates a file called merge_output.json in the job's outputdir.
This contains the desired ntuple name format as the key and a list of PFNs as the value.
"""

import os, re, json
from GangaCore.GPI import DiracFile

def make_ntuple_name(job_name):
	# The nickname is everything before the event type in the job name set by scripts/ganga/submit_job
	nickname = re.match(r"(.*)_\d{8}",job_name).group(1)
	return f"{nickname}/{job_name}.root"

def check(j):
	if len(j.subjobs) == 0 or "getOutputDataAccessURLs" not in dir(j.backend):
		return True
	if not os.path.exists(j.outputdir):
		os.mkdir(j.outputdir)
	outputfile = os.path.join(j.outputdir, "merge_output.json")
	filelist = j.backend.getOutputDataAccessURLs()
	ntuple_name = make_ntuple_name(j.name)
	merge_locations = {ntuple_name: filelist}
	print(merge_locations)
	with open(outputfile, "w") as fp:
		json.dump(merge_locations, fp)
	return os.path.exists(outputfile)

