#!/usr/bin/env python
ROOT.gInterpreter.Declare("""
double cos_theta(ROOT::Math::LorentzVector A, ROOT::Math::LorentzVector B, ROOT::Math::LorentzVector C)
{
	auto R = A + B;
	ROOT::Math::Boost CMFrame(R.BoostToCM());
	auto pB_hat = CMFrame(B).Vect().Unit();
	auto pC_hat = CMFrame(C).Vect().Unit();
	return pB_hat.Dot(pC_hat);
}
""")
