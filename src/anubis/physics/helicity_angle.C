// Källén lambda/triangle function
double kallen(double x, double y, double z)
{
	//return x*x + y*y + z*z - 2*x*y - 2*x*z - 2*y*z;
	double _s = x - y - z;
	return _s*_s - 4*y*z;
}
double costheta(double msq_AB, double msq_AC, double M, double mA, double mB, double mC)
{
	double Msq = M*M;
	double msq_A = mA*mA;
	double msq_B = mB*mB;
	double msq_C = mC*mC;
	double part1 = 2*msq_AB*(msq_AC - msq_C - msq_A);
	double part2 = (msq_AB + msq_A - msq_B)*(Msq - msq_AB - msq_C);
	double denominator = std::sqrt(kallen(msq_AB, msq_C, Msq) * kallen(msq_AB, msq_A, msq_B));
	return (part1 - part2)/denominator;
}
double jacobian(double msq_AB, double M, double mA, double mB, double mC)
{
	double Msq = M*M;
	double msq_A = mA*mA;
	double msq_B = mB*mB;
	double msq_C = mC*mC;
	double denominator = kallen(msq_AB, msq_C, Msq) * kallen(msq_AB, msq_A, msq_B);
	return std::abs(2 * msq_AB/std::sqrt(denominator));
}
template<class T>
double costheta(T A, T B, T C)
{
	auto R = A + B;
	for (auto* P : {&A, &B, &C})
	{
		auto beta = P->BoostToCM(R);
		auto boost = ROOT::Math::Boost(beta);
		*P = boost(*P);
	}
	return B.Vect().Unit().Dot(C.Vect().Unit());
}
