def append_four_momenta(df, particles, mass_pattern = "{particle}_M", momentum_pattern = "{particle}_P{i}"):
	"""Append four-momenta of the passed list of particles as four-vector objects to a ROOT dataframe

	:param df:
	:param particles: List of particle names. Elements of this list are used as `{particle}` in `mass_pattern` and `momentum_pattern`.
	:param mass_pattern: Can be a formatted string to pick out a branch, or a number if you prefer to fix the masses
	:param momentum_pattern:
	:returns: The dataframe with the new definitions appended
	:rtype: ROOT.RDataFrame
	"""
	for particle in particles:
		df = df.Define(f"P_{particle}", define_four_momentum(df, particle, mass_pattern.format(**locals()), momentum_pattern))
	return df

def define_four_momentum(particle, new_mass, momentum_pattern = "{particle}_P{i}", momentum_components = ("X", "Y", "Z"), coordinate_system = "PxPyPzM"):
	"""Define a four-momentum object for a particle in an ntuple

	This assumes use with an ntuple that has columns for the space-like components of momentum which differ by `{i}` in the list `momentum_components`

	:param particle:
	:param new_mass:
	:param momentum_pattern:
	:param momentum_components:
	:param coordinate_system:
	:returns: A ROOT-interpretable string constructing a four-momentum object
	:rtype: str
	"""
	assert len(momentum_components) == 3
	P_args = ", ".join([momentum_pattern.format(particle = particle, i = i) for i in momentum_components] + [str(new_mass)])
	return f"ROOT::Math::{coordinate_system}Vector({P_args})"

