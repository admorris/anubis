import hepunits
import importlib.resources
import ROOT
ROOT.gSystem.Load("libMathMore")
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
for filename in ["helicity_angle.C", "legendre_utils.C"]:
	with importlib.resources.path("anubis.physics", filename) as path:
		ROOT.gROOT.LoadMacro(str(path))

class LegendreMoments:

	def __init__(self, a_name, b_name, c_name, M, m_a, m_b, m_c, binning = None, unit = hepunits.GeV, weight_expr = "1"):
		self.a_name = a_name
		self.b_name = b_name
		self.c_name = c_name
		self.unit = unit
		self.M = M
		self.m_a = m_a
		self.m_b = m_b
		self.m_c = m_c
		self.hists = None
		if binning is None:
			self.binning = {}
		else:
			self.binning = binning
		for comb in ["ab", "ac", "bc"]:
			if f"m2_{comb}" not in self.binning:
				bachelor = list(set("abc")-set(comb))[0]
				self.binning[f"m2_{comb}"] = [25, sum([getattr(self, f"m_{p}") for p in comb])**2, (self.M - getattr(self, f"m_{bachelor}"))**2]
			if f"cos_theta_{comb}" not in self.binning:
				self.binning[f"cos_theta_{comb}"] = [25, -1, +1]
		self.weight_expr = weight_expr

	def add_kinematics(self, df):
		for p in [self.a_name, self.b_name, self.c_name]:
			df = df.Define(f"P_{p}", f"ROOT::Math::PxPyPzEVector({p}_PX/{self.unit}, {p}_PY/{self.unit}, {p}_PZ/{self.unit}, {p}_PE/{self.unit})")
		for comb in ["ab", "ac", "bc"]:
			bachelor = list(set("abc")-set(comb))[0]
			i,j,k = [getattr(self, f"{x}_name") for x in [comb[0], comb[1], bachelor]]
			df = df.Define(f"cos_theta_{comb}", f"costheta(P_{i}, P_{j}, P_{k})")
			df = df.Define(f"m2_{comb}", f"(P_{i}+P_{j}).M2()")
		return df

	def add_moments(self, df, max_order = 4):
		self.max_order = max_order
		self.hists = []
		df = self.add_kinematics(df)
		for i in range(max_order+1):
			df = df.Define(f"lw_{i}", f"{i}.5 * {self.weight_expr} * ROOT::Math::legendre({i}, cos_theta_ab)")
			hist_model = ROOT.RDF.TH1DModel(f"hist_{i}", "", *self.binning["m2_ab"])
			self.hists += [df.Histo1D(hist_model, "m2_ab", f"lw_{i}")]
		self.data_hist = df.Define("weight",f"{self.weight_expr}").Histo1D(ROOT.RDF.TH1DModel("data_hist_tmp", "", *self.binning["m2_ab"]), "m2_ab", "weight").Clone("data_hist")
		self.norm_hists = []
		for i,h in enumerate(self.hists):
			norm_h = h.Clone(f"norm_hist_{i}")
			norm_h.Scale(2) # cosθ runs from −1 to +1
			norm_h.Divide(self.data_hist)
			self.norm_hists += [norm_h]
		print(f"Moments calculated with max_order = {self.max_order}")
		return df

	def add_mc_weight(self, df):
		df = self.add_kinematics(df)
		self.MC_hist = df.Histo1D(ROOT.RDF.TH1DModel("MC_hist_tmp", "", *self.binning["m2_ab"]), "m2_ab").Clone("MC_hist")
		return df.Define("legendre_weight", f"angular_weight(m2_ab, cos_theta_ab, {self.max_order})")

	def make_comparison_hists(self, data_df, mc_df, quantity = "cos_theta_ab"):
		hist_model = ROOT.RDF.TH1DModel("hist", "", *self.binning[quantity],)
		mc_hist = mc_df.Histo1D(hist_model, quantity, "legendre_weight")
		data_hist = data_df.Define("weight",f"{self.weight_expr}").Histo1D(hist_model, quantity, "weight")
		return data_hist, mc_hist

	def make_slices(self, df, quantity = "cos_theta_ab"):
		if self.hists is None:
			print("WARNING: add_moments() not yet called. Calculating moments with default parameters.")
			self.add_moments(df)
		hist = df.Histo2D(ROOT.RDF.TH2DModel(f"m2_ab_vs_{quantity}", "", *self.binning["m2_ab"], *self.binning[quantity]), "m2_ab", quantity)
		hists = []
		funcs = []
		for j in range(1, hist.GetNbinsX()+1):
			eval_func = {
				"m2_ab": f"find_bin(x) == {j} ? get_data_hist()->GetBinContent({j}) : 0",
				"m2_ac": f"msq_AC_distribution({j}, x, {self.M}, {self.m_a}, {self.m_b}, {self.m_c}, {self.max_order})/{hist.GetNbinsY()}.",
				"cos_theta_ab": f"angular_distribution({j}, x, {self.max_order})/{hist.GetNbinsY()}.",
			}[quantity]
			funcs += [ROOT.TF1(f"f_{quantity}_bin{j}", eval_func, *self.binning[quantity][1:])]
			hists += [hist.ProjectionY(f"h_{quantity}_bin{j}", j, j, "eo")]
			#
		return hists, funcs

	def make_projection(self, df, quantity = "cos_theta_ab"):
		hist = df.Histo1D(ROOT.RDF.TH1DModel(f"{quantity} projection", "", *self.binning[quantity]), quantity)
		eval_func = {
			"m2_ab": "get_data_hist()->GetBinContent(find_bin(x))",
			"m2_ac": f"msq_AC_projection(x, {self.M}, {self.m_a}, {self.m_b}, {self.m_c}, {self.max_order})/{hist.GetNbinsX()}.",
			"cos_theta_ab": f"angular_projection(x, {self.max_order})/{hist.GetNbinsX()}.",
		}[quantity]
		func = ROOT.TF1(f"{quantity} function", eval_func, *self.binning[quantity][1:])
		return hist, func
