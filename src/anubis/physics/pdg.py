from particle import Particle
from particle.exceptions import MatchingIDNotFound

PID_CACHE = {}

def get_particle(pid):
	"""Create a Particle object from the sci-kit hep `particle` package, given a name or PDG ID
	
	This wrapper function was born out of the slow performance of `Particle.from_string`.
	If the argument is a string not present in the dict `PID_CACHE`, then the much quicker method `Particle.from_evtgen_name` will be tried first.
	If the particle is subsequently found by `Particle.from_string`, the name is associted with a PDG ID in the dict `PID_CACHE` in order to speed up future calls.
	:param pid: A name or a PDG ID
	:type pid: str or int
	:returns: A `Particle` object
	:rtype: particle.Particle
	"""
	if type(pid) == str:
		if pid in PID_CACHE:
			pid = PID_CACHE[pid]
		else:
			try:
				p = Particle.from_evtgen_name(pid)
			except MatchingIDNotFound:
				p = Particle.from_string(pid)
				PID = int(p.pdgid)
				print(f"WARNING: The particle '{pid}' was found using super-slow Particle.from_string(). "
				f"Performance would be improved if you used the EvtGen name '{p.evtgen_name}' or PID {PID} instead! "
				"The PID will be cached to speed up future calls.", file=sys.stderr)
				PID_CACHE[pid] = PID
			return p
	return Particle.from_pdgid(pid)
