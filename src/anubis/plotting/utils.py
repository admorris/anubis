#!/usr/bin/env python
import ROOT

CANV_W = 1600
CANV_H = 900
PULL_F = 0.25
PULL_SCALE_F = 1/PULL_F - 1

def scale_text(plot, scale_factor):
	for axis in ["x","y"]:
		plot.SetTitleSize(scale_factor*ROOT.gStyle.GetTitleSize(axis), axis)
		plot.SetLabelSize(scale_factor*ROOT.gStyle.GetLabelSize(axis), axis)
	plot.SetTitleOffset(ROOT.gStyle.GetTitleOffset("y")/scale_factor, "y")
	plot.SetTickLength(ROOT.gStyle.GetTickLength("x")*scale_factor, "x")

def style_roofit_pulls(hist, plot):
	plot.SetYTitle("Pull")
	plot.GetYaxis().CenterTitle()
	plot.SetNdivisions(5, "y")
	hist.SetFillColor(ROOT.kBlack)
	hist.SetLineWidth(0)
	plot.SetTitle("")
	maxpull = max([-plot.GetMinimum(), plot.GetMaximum(), 6])
	plot.SetMinimum(-maxpull)
	plot.SetMaximum( maxpull)
	scale_text(plot, PULL_SCALE_F)
#	plot.SetLabelOffset(0.25*plot.GetYaxis().GetLabelOffset(), "y")
	plot.SetLabelSize(0.6*plot.GetYaxis().GetLabelSize(), "y")
	for nsigma in [2]:
		b = ROOT.TBox(plot.GetXaxis().GetXmin(), -nsigma, plot.GetXaxis().GetXmax(), +nsigma)
		b.SetLineColor(ROOT.kGray+2)
		b.SetLineStyle(ROOT.kDotted)
		b.Draw()
		ROOT.gDirectory.Add(b)

def create_plot_canvas():
	c = ROOT.TCanvas("can", "", CANV_W, int(CANV_H/(1.-ROOT.gStyle.GetPadBottomMargin())))
	mainpad = ROOT.TPad("mainpad", "", 0.00, 0.00, 1.00, 1.00)
	mainpad.Draw()
	return c,mainpad

def create_plot_pull_canvas():
	c = ROOT.TCanvas("can", "", CANV_W, int(CANV_H/(1.-PULL_F)))
	mainpad = ROOT.TPad("mainpad", "", 0.00, PULL_F, 1.00, 1.00)
	mainpad.Draw()
	mainpad.SetBottomMargin(0)
	pullpad = ROOT.TPad("pullpad", "", 0.00, 0.00, 1.00, PULL_F)
	pullpad.SetFrameFillColor(ROOT.kGray)
	pullpad.SetFrameFillStyle(4000)
	pullpad.SetFillStyle(4000)
	pullpad.Draw()
	pullpad.SetTopMargin(0)
	pullpad.SetBottomMargin(pullpad.GetBottomMargin()*PULL_SCALE_F)
	return c,mainpad,pullpad

def bin_function(f, ax):
	h = ROOT.TH1D(f"{f.GetName()}_hist", "", ax.GetNbins(), ax.GetXmin(), ax.GetXmax())
	for j in range(1, ax.GetNbins()+1):
		a = ax.GetBinLowEdge(j)
		b = ax.GetBinUpEdge(j)
		val = f.Integral(a,b)/h.GetBinWidth(j)
		#err = f.IntegralError(a,b)
		h.SetBinContent(j, val)
		#h.SetBinError(j, err)
	return h

def make_pulls(h1, h2):
	ratio = h1.Integral() / h2.Integral()
	if ratio < 0.9 or ratio > 1.1:
		print(f"WARNING: ratio of integrals {h1.GetName()}/{h2.GetName()} = {ratio}")
	resid = h1.Clone("{}_{}_residual".format(h1.GetName(), h2.GetName()))
	resid.Add(h1, h2, 1, -1)
	return pullplot(resid)

def pullplot(h):
	hist = h.Clone(h.GetName()+"_pull")
	for i in range(hist.GetNbinsX()):
		if h.GetBinError(i+1) > 0:
			pull = h.GetBinContent(i+1)/h.GetBinError(i+1)
		else:
			pull = 0
		hist.SetBinContent(i+1, pull)
	style_roofit_pulls(hist, hist)
	return hist

def y_axis_title(x_ax, quantity = "Candidates", unit = ""):
	return f"{quantity} / ({x_ax.GetBinWidth(1):.3f} {unit})".replace(" )", ")")

def x_axis_title(quantity, unit = ""):
	return f"{quantity} [{unit}]".replace(" []", "")
