#!/usr/bin/env python
import ROOT

def apply_cut(fin, path, cuts, fout, append = False):
	opts = ROOT.RDF.RSnapshotOptions()
	if append:
		opts.fMode = "UPDATE"
		opts.fOverwriteIfExists = True
	df = ROOT.RDataFrame(path, fin)
	for key in cuts:
		df = df.Filter(cuts[key], key)
	df.Report().Print()
	df.Snapshot(path, fout, "", opts)

def main():
	import argparse
	parser = argparse.ArgumentParser(description = "Copy a TTree to a new TFile while applying cuts")
	parser.add_argument("input", help = "Input file")
	parser.add_argument("path", help = "Path to tree within file")
	parser.add_argument("cut", help = "Cut string")
	parser.add_argument("output", help = "Output file")
	parser.add_argument("--append", action="store_true", help="Add the new TTree to the output file instead of overwriting the file")
	args = parser.parse_args()
	apply_cut(args.input, args.path, {"cut": args.cut}, args.output, args.append)

if __name__ == "__main__":
	main()

