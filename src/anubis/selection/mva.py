#!/usr/bin/env python
import array
import importlib.resources
import ROOT
for filename in ["plotall.C", "opt_mva.C"]:
	with importlib.resources.path("anubis.selection", filename) as path:
		ROOT.gROOT.LoadMacro(str(path))

def load(loader, name, files, path, cut, weight):
	"""

	:param loader:
	:param name:
	:param files:
	:param path:
	:param cut:
	:param weight:
	"""
	tree = ROOT.TChain(path)
	for f in files:
		tree.Add(f)
	loader.AddTree(tree, name, 1, ROOT.TCut(cut))
	if weight != "":
		loader.SetSignalWeightExpression(weight)

def validate_sample_dict(sample_dict):
	"""

	:param sample_dict:
	:type  sample_dict: dict[str: *]
	"""
	# Look for mandatory keys
	for key in "files", "path":
		if key not in sample_dict:
			raise ValueError(f"The dict defining the training samples must contain the key '{key}'")
	# Set optional keys to empty string if not present
	for key in "cut", "weight":
		if key not in sample_dict:
			sample_dict[key] = ""

def train(signal, background, methods, variables, spectators = [], name = "TMVA", factory_config = "AnalysisType=Classification:!DrawProgressBar:Silent", split_config = "SplitMode=Random:NormMode=NumEvents", cross_validate = False):
	"""Train several MVA algorithms on the same signal and background samples.

	:param signal:         Information about the signal sample. Must contain the keys "files" (a list of strings corresponding to filenames or glob expressions) and "path" (the location of the TTree object inside the TFile). Optional keys are "cut" and "weight", which are both TTreeFormula-interpretable strings.
	:type  signal:         dict
	:param background:     Information about the background sample. Must conform to the same format as signal.
	:type  background:     dict
	:param methods:        A list of tuples: the name of the enum for the algorithms type (e.g. kBDT, kMLP), a string for the name and a second string for the algorithm options. These will be expanded and passed as the 2nd, 3rd and 4th arguments to TMVA::Factory::BookMethod or TMVA::CrossValidation::BookMethod.
	:type  methods:        list[tuple[str, str, str]]
	:param variables:      List of training variables. Each one is passed to TMVA::DataLoader::AddVariable.
	:type  variables:      list[str]
	:param spectators:     Optional list of spectator variables and their types. Each one is expanded and passed as the arguments to TMVA::DataLoader::AddSpectator. If using cross-validation, you will probably need to add the variable(s) used in the splitting expression here.
	:type  spectators:     list[tuple[str, str]]
	:param name:           The name of the TMVA::DataLoader and the output file.
	:type  name:           str
	:param factory_config: Options string passed to TMVA::Factory or TMVA::CrossValidation.
	:type  factory_config: str
	:param split_config:   Options string passed to TMVA::DataLoader::PrepareTrainingAndTestTree
	:type  split_config:   str
	:param cross_validate: Use TMVA::CrossValidation instead of TMVA::Factory.
	:type  cross_validate: bool
	"""
	validate_sample_dict(signal)
	validate_sample_dict(background)
	#
	ROOT.TMVA.Tools.Instance()
	#
	output_file = ROOT.TFile.Open(f"{name}.root", "RECREATE")
	#
	loader = ROOT.TMVA.DataLoader(name)
	#
	for variable in variables:
		loader.AddVariable(variable)
	for spectator in spectators:
		loader.AddSpectator(*spectator)
	#
	load(loader, "Background", background["files"], background["path"], background["cut"], background["weight"])
	load(loader, "Signal", signal["files"], signal["path"], signal["cut"], signal["weight"])
	#
	loader.PrepareTrainingAndTestTree(ROOT.TCut(), split_config)
	#
	for i, method in enumerate(methods):
		methods[i][0] = getattr(ROOT.TMVA.Types, method[0])
	if(cross_validate):
		cv = ROOT.TMVA.CrossValidation("TMVAClassification", loader, output_file, factory_config)
		for method in methods:
			cv.BookMethod(*method)
		cv.Evaluate()
	else:
		factory = ROOT.TMVA.Factory("TMVAClassification", output_file, factory_config)
		for method in methods:
			factory.BookMethod(loader, *method)
		factory.TrainAllMethods()
		factory.TestAllMethods()
		factory.EvaluateAllMethods()
	output_file.Close()

def append(df, methods, variables, spectators = [], name = "TMVA"):
	"""Append the MVA algorithm scores to an ntuple.

	:param df:         Input dataframe
	:type  df:         ROOT.RDataFrame
	:param methods:    A list of tuples: an enum for the algorithms type, a string for the name and a second string for the algorithm options. See :train():.
	:type  methods:    list[tuple[int, str, str]]
	:param variables:  List of training variables. Each one is passed to TMVA::DataLoader::AddVariable.
	:type  variables:  list[str]
	:param spectators: Optional list of spectator variables and their types. Each one is expanded and passed as the arguments to TMVA::DataLoader::AddSpectator. If using cross-validation, you will probably need to add the variable(s) used in the splitting expression here.
	:type  spectators: list[tuple[str, str]]
	:param name:       The name of the TMVA::DataLoader and the output file.
	:type  name:       str
	:returns:          The dataframe with new columns for the MVA scores.
	:rtype:            ROOT.RDataFrame
	"""
	ROOT.TMVA.Tools.Instance()
	reader = ROOT.TMVA.Reader("reader")
	for i, method in enumerate(methods):
		methods[i][0] = method[0].strip("k")
	all_variable_names = []
	for v in variables + spectators:
		if ":=" in v:
			v_name, v_def = v.split(":=")
			v_name = v_name.strip()
			df = df.Define(v_name, v_def)
		else:
			v_name = v
		all_variable_names += [v_name]
	for alg_type, alg_name, alg_opts in methods:
		ROOT.gInterpreter.Declare(f"""
		TMVA::Experimental::RReader {alg_name}_reader("{name}/weights/TMVAClassification_{alg_name}.weights.xml");
		float predict_{alg_name} (const std::vector<double> values)
		{{
			std::vector<float> values_f;
			for(auto&& v: values)
				values_f.push_back(v);
			return {alg_name}_reader.Compute(values_f)[0];
		}}
		""")
		df = df.Define(alg_name, f"""predict_{alg_name}({{{", ".join(all_variable_names)}}})""")
	return df

def plotall(filename = "TMVA.root"):
	ROOT.plotall(filename)


def opt_mva(filename = "TMVA.root", ns = 1000, nb = 1000):
	ROOT.opt_mva(filename, ns, nb)

