import re, sys, os
from copy import copy
import ROOT
import numpy as np
from hepunits import MeV, GeV
from ..physics.pdg import get_particle
from ..physics.kinematics import *

class Background:
	"""
	"""
	def __init__(self, parent, decay_products, conjugate = False, veto = False, window = 40 * MeV):
		"""

		:param parent:
		:param decay_products:
		:param conjugate:
		:param veto:
		:param window:
		"""
		self.parent = get_particle(parent)
		self.decay_products = [get_particle(p) for p in decay_products]
		self.decay_products_cc = [p.invert() for p in self.decay_products]
		self.conjugate = conjugate
		self.veto = veto
		self.window = window
		self.cut_template = "abs({mass_branch} - {self.parent.mass}) > {self.window}"
	def __str__(self):
		"""LaTeX version of the decay

		:returns:
		:rtype: str
		"""
		return self.parent.latex_name + " \\to " + " ".join(p.latex_name for p in self.decay_products)
	def cutstring(self, mass_branch):
		"""Cutstring to apply the mass veto

		:param mass_branch:
		:returns:
		:rtype: str
		"""
		return self.cut_template.format(self = self, mass_branch = mass_branch)

class HypothesisCollection:
	"""
	"""
	def __init__(self, branches, original, alternatives, mass_pattern = "{particle}_M", momentum_pattern = "{particle}_P{i}", expression_pattern = "{mass_branch}"):
		"""

		:param branches:
		:param original:
		:param alternatives:
		:param mass_pattern:
		:param momentum_pattern:
		:param expression_pattern:
		"""
		assert len(branches) == len(original)
		self.branches = branches
		self.original = [get_particle(org) for org in original]
		self.alternatives = [list(self.build_hypothesis(alt)) for alt in alternatives]
		self.P_definitions = {}
		self.mass_definitions = {}
		self.mass_expressions = {}
		self.combination_latex = {}
		for hyp in self.alternatives:
			names = []
			for branch, org, alt in zip(self.branches, self.original, hyp):
				name = self.build_particle_name(branch, org, alt)
				self.P_definitions[f"P_{name}"] = define_four_momentum(branch, mass_pattern.format(particle = branch)+f"/{GeV}" if org == alt else alt.mass/GeV, momentum_pattern+f"/{GeV}")
				names += [name]
			comb_name = self.build_combination_name(hyp)
			self.mass_definitions[f"{comb_name}_M"] = "(" + " + ".join([f"P_{name}" for name in names]) + f").M()*{GeV}"
			self.mass_expressions[comb_name] = expression_pattern.format(mass_branch = f"{comb_name}_M")
			self.combination_latex[comb_name] = self.build_combination_latex(hyp)
	def __str__(self):
		"""String representation of the original and alternative hypotheses

		:returns:
		:rtype: str
		"""
		org_str = " ".join([p.name for p in self.original])
		alt_strs = [" ".join([p.name for p in alt]) for alt in self.alternatives]
		return f"Original mass hypothesis: {org_str}. Alternative hypotheses: {alt_strs}."
	def build_hypothesis(self, names):
		"""

		:param names:
		:returns:
		:rtype: str
		"""
		assert len(names) == len(self.original)
		for org, alt in zip(self.original, names):
			if alt == "same":
				yield org
			else:
				yield get_particle(alt)
	def build_particle_name(self, branch, original, alternative):
		"""

		:param branch:
		:param original:
		:param alternative:
		:returns:
		:rtype: str
		"""
		if original == alternative:
			return branch
		else:
			return branch + "_as_" + alternative.programmatic_name
	def build_combination_name(self, hypothesis):
		"""

		:param hypothesis:
		:returns:
		:rtype: str
		"""
		assert len(hypothesis) == len(self.original)
		names = [self.build_particle_name(*args) for args in zip(self.branches, self.original, hypothesis)]
		return "_".join(names)
	def build_combination_latex(self, hypothesis):
		"""

		:param hypothesis:
		:returns:
		:rtype: str
		"""
		assert len(hypothesis) == len(self.original)
		names = [particle.latex_name for particle in hypothesis]
		return " ".join(names)

class PeakingBackgroundStudy:
	"""
	"""
	def __init__(self, hypothesis_collections, backgrounds):
		"""

		:param hypothesis_collections:
		:type hypothesis_collections: list of HypothesisCollection objects
		:param backgrounds:
		:type backgrounds: list of Background objects
		"""
		self.backgrounds = []
		self.hypothesis_collections = []
		self.P_definitions = {}
		self.mass_definitions = {}
		for h_c in hypothesis_collections:
			self.add_hypothesis(h_c)
		for bg in backgrounds:
			self.add_background(bg)
		self.cutstring = self.build_cutstring()
	def add_hypothesis(self, h_c):
		"""

		:param h_c:
		:type h_c: HypothesisCollection
		"""
		self.hypothesis_collections += [h_c]
		for branch, defn in h_c.P_definitions.items():
			self.P_definitions[branch] = defn
		for branch, defn in h_c.mass_definitions.items():
			self.mass_definitions[branch] = defn
	def add_background(self, bg):
		"""

		:param bg:
		:type bg: Background
		"""
		self.backgrounds += [bg]
	def append_definitions(self, df):
		"""

		:param df:
		:type df: ROOT.RDataFrame
		:returns:
		:rtype: ROOT.RDataFrame
		"""
		for branch, defn in self.P_definitions.items():
			df = df.Define(branch, defn)
		for branch, defn in self.mass_definitions.items():
			df = df.Define(branch, defn)
		return df
	def get_mass_branches(self):
		"""Get a list of newly defined mass branches. Intended to be passed to RDF.Snapshot()

		:returns:
		:rtype: list of str
		"""
		return [branch for branch in self.mass_definitions]
	def build_cutstring(self, excluding = []):
		"""

		:param excluding: Exclude cuts on the mass hypotheses contained in this list
		:type excluding: list
		:returns:
		:rtype: str
		"""
		cutstrings = []
		for h_c in self.hypothesis_collections:
			for hyp in h_c.alternatives:
				comb_name = h_c.build_combination_name(hyp)
				if comb_name in excluding: continue
				mass_branch = h_c.mass_expressions[comb_name]
				for bg in self.backgrounds:
					if bg.veto and (sorted(bg.decay_products) == sorted(hyp) or bg.conjugate and sorted(bg.decay_products_cc) == sorted(hyp)):
						cutstrings += [bg.cutstring(mass_branch)]
		if len(cutstrings) == 0:
			return "1"
		else:
			return " && ".join(cutstrings)
	def draw_mass_lines(self, comb_name, ypos):
		"""Draw vertical lines at the mass of each parent particle in the set of peaking backgrounds for a given mass hypothesis

		"""
		for h_c in self.hypothesis_collections:
			for hyp in h_c.alternatives:
				if comb_name == h_c.build_combination_name(hyp):
					for bg in self.backgrounds:
						if (sorted(bg.decay_products) == sorted(hyp) or bg.conjugate and sorted(bg.decay_products_cc) == sorted(hyp)):
							xpos = bg.parent.mass * MeV
							line = ROOT.TLine(xpos, 0, xpos, ypos)
							line.SetLineColor(ROOT.kBlack)
							line.SetLineStyle(ROOT.kDotted)
							line.Draw()
							ROOT.TLatex().DrawLatex(xpos, ypos, bg.parent.latex_name)
							yield line
	def plot_all(self, df, folder = os.getcwd(), extension = "png", bin_width = 5 * MeV, percentiles = (0, 100)):
		"""Create one plot per mass hypothesis

		"""
		if not os.path.exists(folder): os.makedirs(folder)
		hists = []
		branch_minima = {}
		branch_maxima = {}
		combinations = []
		# Unline TTree::Draw(), RDataFrame's Histo1D doesn't offer a nice "auto-same" feature
		# So here we use two loops to deduce the binning scheme ourselves
		# The first loop sets up the lazy actions needed in the second loop, which is significantly quicker than calling instant actions in one loop
		# Likewise, the second loop sets up the lazy actions needed for the third loop, which performs the histogram drawing
		tmp_df = df.Filter("1") # Need a copy of the dataframe
		for h_c in self.hypothesis_collections:
			for hyp in h_c.alternatives:
				comb_name = h_c.build_combination_name(hyp)
				combinations += [(h_c, comb_name)]
				column_name = f"{comb_name}_mass_expression"
				tmp_df = tmp_df.Define(column_name, h_c.mass_expressions[comb_name])
		print("Calculating histogram limits")
		all_mass_expressions = tmp_df.AsNumpy([f"{comb_name}_mass_expression" for h_c, comb_name in combinations])
		for h_c, comb_name in combinations:
			xlow,xup = np.percentile(all_mass_expressions[f"{comb_name}_mass_expression"], percentiles)
			nbins = int((xup-xlow)/bin_width)
			xup = xlow + nbins*bin_width
			hist_model = ROOT.RDF.TH1DModel(f"hist_{comb_name}", "", nbins, xlow, xup)
			hists += [self.plot(tmp_df, h_c, comb_name, hist_model)]
		for comb_name,x_title,h_before,h_exclusive,h_after in hists:
			print(f"Plotting {comb_name}")
			c = ROOT.TCanvas(f"canv_{comb_name}", "", 1200, 900)
			h_before.Draw()
			h_before.SetMinimum(0)
			old_ymax = h_before.GetMaximum()
			h_before.SetMaximum(old_ymax*1.2) # Make some room at the top for particle labels
			h_before.GetXaxis().SetTitle(x_title)
			h_exclusive.Draw("same")
			h_after.Draw("same")
			for hist, colour in [(h_before, ROOT.kGreen), (h_exclusive, ROOT.kRed), (h_after, ROOT.kBlue)]:
				hist.SetFillStyle(3002)
				hist.SetFillColor(colour)
				hist.SetLineColor(colour)
			lines = list(self.draw_mass_lines(comb_name, old_ymax*1.1))
			c.SaveAs(os.path.join(folder, f"{comb_name}.{extension}"))
	def plot(self, df, h_c, comb_name, hist_model):
		"""
		"""
		exclusive_cutstring = self.build_cutstring(excluding = [comb_name])
		h_before = df.Histo1D(hist_model, f"{comb_name}_mass_expression")
		h_exclusive = df.Filter(exclusive_cutstring).Histo1D(hist_model, f"{comb_name}_mass_expression")
		h_after = df.Filter(self.cutstring).Histo1D(hist_model, f"{comb_name}_mass_expression")
		return (comb_name, f"m({h_c.combination_latex[comb_name]}) MeV", h_before, h_exclusive, h_after)

