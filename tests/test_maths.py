def test_rounding():
	from anubis.maths.round import pdg_round
	# Test PDG rounding using their examples
	assert(pdg_round(0.827, 0.119) == ("0.83", "0.12"))
	assert(pdg_round(0.827, 0.367) == ("0.8", "0.4"))
	assert(pdg_round(0.827, 0.096) == ("0.83", "0.10"))
	try:
		pdg_round(0.827, 0.0)
	except ValueError:
		pass